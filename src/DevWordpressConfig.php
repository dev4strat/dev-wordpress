<?php
/**
 * Create wordpress config file with composer.
 *
 * @package devWordpress
 * @since   2024
 */

namespace Dev4strat\DevWordpress;

use ErrorException;

/**
 * Wordpress Config.
 * Replace config file.
 * Create style.css for theme.
 */
class DevWordpressConfig {
    public string $file;
    public string $content;

    /**
     * Constructor.
     */
    public function __construct() {
        $this->changeWpConfig();
    }

    /**
     * Change wp config.
     *
     * @return void
     */
    private function changeWpConfig(): void
    {
        $path = './out/wp-config.php';

        if ( file_exists( $path ) ) {
            $content = file_get_contents( $path );
            $lines   = explode( "\n", $content );
            foreach ( $lines as $index => $line ) {
                if ( str_contains( $line, 'DB_HOST' ) ) {
                    $this->replaceLineWpConfig( $path, $line );
                }
            }
        }
    }

    /**
     * Get Ip address.
     *
     * @return string|null
     */
    private function getIp(): ?string {
        try {
            return shell_exec( 'ipconfig getifaddr en0' );
        } catch ( ErrorException $e ) {
            return null;
        }
    }

    /**
     * Get Port.
     *
     * @return array
     */
    private function getPort(): string {
        global $argv;
        $args = $this->arguments( $argv );
        return $args['port'];
    }

    /**
     * Replace Lines in wp config.
     *
     * @param string $path Path of wp config file.
     * @param string $line line of file.
     * @return void
     */
    private function replaceLineWpConfig( string $path, string $line ): void
    {
        $host = $this->getIp() ? trim( $this->getIp() ) . ':' . $this->getPort() : 'mysqldb';
        file_put_contents(
            $path,
            str_replace(
                $line,
                "define( 'DB_HOST', '" . $host . "' );",
                file_get_contents( $path )
            )
        );

        DevHelper::println( 'MYSQL Host: ' . $host );
    }

    /**
     * Get global variables.
     *
     * @param array $argv Arguments.
     * @return array
     */
    private function arguments( array $argv ): array {
        $_arg = array();
        foreach ( $argv as $arg ) {
            if ( preg_match( '#^-{1,2}([a-zA-Z0-9]*)=?(.*)$#', $arg, $matches ) ) {
                $key = $matches[1];
                switch ( $matches[2] ) {
                    case '':
                    case 'true':
                        $arg = true;
                        break;
                    case 'false':
                        $arg = false;
                        break;
                    default:
                        $arg = $matches[2];
                }
                $_arg[ $key ] = $arg;
            } else {
                $_arg['input'][] = $arg;
            }
        }
        return $_arg;
    }
}
