<?php

namespace DevTest;

use Composer\Script\Event;
use Dev4strat\DevWordpress\DevWordpress;

/**
 *
 */
class DevTest extends DevWordpress
{
    /**
     * @param Event $event
     * @return void
     */
    public static function testCopy(Event $event): void
    {
        parent::getRoot($event);
        parent::loadEnv();

        self::copy(self::$project_path.'/src/Inc/', self::$project_path.'/out/Inc/');
        self::copy(self::$project_path.'/src/DevTest/config', self::$project_path.'/out/Inc/test');
    }
}
