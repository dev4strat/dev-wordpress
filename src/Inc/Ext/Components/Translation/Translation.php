<?php
/**
 * Translation Helper.
 *
 * @package dev4strat
 * @since   2024
 */

namespace Inc\Ext\Components\Translation;

/**
 * Translation Helper for clipboard text-domain.
 */
class Translation
{
    public function __construct()
    {
        if ( ! defined('TEXT_DOMAIN') ) {
            define('TEXT_DOMAIN', '');
            wp_die('TEXT_DOMAIN is missing in wp-config', 'Translation', array( 'code' => 'Error' ));
        }

        // Set TEXT_DOMAIN in wp-config
        add_filter('gettext_' . TEXT_DOMAIN, array($this, 'filter_texts'), 10, 3);
        add_filter('esc_html', array($this, 'sanitize_html'), 10, 2);
        add_filter('admin_bar_menu', array($this, 'admin_menu'));
        add_action('wp_enqueue_scripts', array($this, 'load_styles'));
    }

    /**
     * Filters text with its translation.
     *
     * @param string $translation Translated text.
     * @param string $text Text to translate.
     * @param string $domain Text domain. Unique identifier for retrieving translated strings.
     * @return string
     */
    public function filter_texts(string $translation, string $text, string $domain): string
    {
        if (isset($_GET['t']) && current_user_can('administrator') ) {
            return $translation . $this->clipboard_element($text);
        }

        return $translation;
    }

    /**
     * Template.
     *
     * @param string $text Text.
     * @return string
     */
    private function clipboard_element(string $text): string
    {
        return sprintf(
            ' <span class="clipboard btn btn-outline-primary p-0">
                        <span class="clipboard-value d-none">%s</span>
                        <span class="dashicons dashicons-translation"></span>
                    </span> ',
            esc_html(str_replace('%', '%%', $text))
        );
    }

    /**
     * Filters a string cleaned and escaped for output in HTML.
     *
     * Text passed to esc_html() is stripped of invalid or special characters
     * before output.
     *
     * @param string $safe_text The text after it has been escaped.
     * @param string|null $text The text prior to being escaped.
     * @return string
     * @since 2.8.0
     *
     */
    public function sanitize_html(string $safe_text, ?string $text): string
    {
        if (isset($_GET['t']) && current_user_can('administrator') && $text) {
            return strip_tags($text, ['<span>', '</span>']);
        }

        return $safe_text;
    }

    /**
     * Add menu in admin bar.
     *
     * @param mixed $admin_bar Admin bar.
     */
    public function admin_menu(mixed $admin_bar): void
    {
        if (current_user_can('administrator')) {
            $admin_bar->add_menu(
                array(
                    'id' => 'translation-menu',
                    'parent' => 'top-secondary',
                    'title' => isset($_GET['t']) ? __('Hide translation') : __('Show translation'),
                    'href' => isset($_GET['t']) ? '?' : '?t=1',
                    'meta' => array(
                        'title' => __('Show translation'),
                    ),
                )
            );

            $admin_bar->add_menu(
                array(
                    'id' => 'loco-translation-menu',
                    'parent' => 'top-secondary',
                    'title' => 'Loco translate',
                    'href' => admin_url('admin.php?bundle=' . TEXT_DOMAIN . '&page=loco-theme&action=view'),
                    'meta' => array(
                        'title' => 'Loco translate',
                    ),
                )
            );
        }
    }

    /**
     * Add styles.
     *
     * @return void
     */
    public function load_styles(): void
    {
        if (isset($_GET['t']) && current_user_can('administrator')) {
            wp_enqueue_style('dashicons');
            $js_file = '/Inc/Ext/Components/Translation/translation.js';

            if (!file_exists(get_template_directory() . $js_file)) {
                $js_file = '/inc/Ext/Components/Translation/translation.js';
            }

            wp_enqueue_script('translation', get_template_directory_uri() . $js_file, array(), 1, true);
        }
    }
}
