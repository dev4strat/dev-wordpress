window.onload = function() {
    var translationDomains = document.querySelectorAll('.clipboard');
    const alertClass = 'translation-alert';

    for (var i = 0; i < translationDomains.length; i++) {
        translationDomains[i].addEventListener('click', function(e) {
            e.preventDefault();
            const clipboardValue = this.querySelector('.clipboard-value').innerHTML;
            navigator.clipboard.writeText(clipboardValue);
            alert(clipboardValue);
            remove();
        }, false);
    }

    function alert(clipboardValue) {
        const parentDiv = document.createElement("div");
        parentDiv.classList.add(alertClass);
        parentDiv.style.position = 'fixed';
        parentDiv.style.bottom = '2rem';
        parentDiv.style.display = 'flex';
        parentDiv.style.width = '100%';
        parentDiv.style.zIndex = 9999;

        const alertDiv = document.createElement("div");
        alertDiv.classList.add('alert');
        alertDiv.style.marginLeft = 'auto';
        alertDiv.style.backgroundColor = 'white';
        alertDiv.style.padding = '1rem';
        alertDiv.innerText = 'Clipboard: ' + clipboardValue;

        parentDiv.append(alertDiv);
        document.body.append(parentDiv);
    }

    function remove() {
        setTimeout(function () {
            document.querySelectorAll('.' + alertClass).forEach(function (e) {
                e.remove();
            })
        }, 2000);
    }
};
