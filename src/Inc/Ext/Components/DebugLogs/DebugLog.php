<?php
/**
 * Debug log model.
 *
 * @package WordPress
 * @subpackage 4strat
 * @since 4strat 2024
 */

namespace Inc\Ext\Components\DebugLogs;

/**
 * Debug log model.
 */
class DebugLog {
    /**
     * Constructor.
     *
     * @param int|null    $id Id.
     * @param int|null    $count Count.
     * @param string|null $time Time.
     * @param string|null $message Message.
     * @param string|null $type type.
     */
    public function __construct(
        public ?int $id = null,
        public ?int $count = null,
        public ?string $time = null,
        public ?string $message = null,
        public ?string $type = null,
    ) { }
}
