<?php
/**
 * Print debug logs.
 *
 * @package WordPress
 * @subpackage 4strat
 * @since 4strat 2024
 */

namespace Inc\Ext\Components\DebugLogs;

use DateTimeInterface;

/**
 * Class debug logs.
 */
class DebugLogsPrint {
    /**
     * @var false|resource
     */
    private mixed $file;

    /**
     * Constructor.
     */
    public function __construct(
        private ?string $file_path = null,
        private ?array $content = null,
        private ?array $cols = null
    ) {
        add_action( 'admin_menu', array( $this, 'menu' ) );
        add_filter('admin_bar_menu', array($this, 'barMenu'));
    }

    /**
     * Menu.
     *
     * @return void
     */
    public function menu(): void {
        add_options_page('Debug logs', 'Debug logs', 'activate_plugins', 'debug_logs', array( $this, 'settingsPage'), 10);
    }

    /**
     * Add menu in admin bar.
     *
     * @param mixed $admin_bar Admin bar.
     */
    public function barMenu(mixed $admin_bar): void {
        if (current_user_can('administrator')) {
            $admin_bar->add_menu(
                array(
                    'id' => 'debug-log-menu',
                    'parent' => 'top-secondary',
                    'title' => __('Debug logs'),
                    'href' => admin_url('/options-general.php?page=debug_logs'),
                    'meta' => array(
                        'title' => __('Debug logs'),
                    ),
                ),
            );
        }
    }

    /**
     * Settings page display callback.
     */
    public function settingsPage(): void {
        $this->file_path = WP_CONTENT_DIR . '/debug.log';

        if (!file_exists( $this->file_path)) {
            return;
        }

        echo '<h1>Debug logs</h1>';
        $table = $this->templateHead();
        $table .= $this->templateBody();
        echo $this->templateTable($table);
    }

    /**
     * @return array
     */
    private function getContents(): array {
        $this->parse($this->get_file());

        $contents = array_values($this->content ?? array());
        fclose($this->file);
        return $contents;
    }

    /**
     * @return bool|string
     */
    private function get_file(): bool|string {
        $this->file = fopen($this->file_path, 'r');
        $length = 1024 * 1024;
        return fread($this->file, $length);
    }

    /**
     * @param $raw
     * @return void
     */
    private function parse($raw): void {
        preg_replace_callback('~^\[([^\]]*)\]((?:[^\r\n]*[\r\n]?(?!\[).*)*)~m', array( $this, 'setContent'), $raw);
    }

    /**
     * @param array $arr.
     * @return void
     */
    private function setContent(array $arr): void {
        $message = trim($arr[2] ?? '');
        if (empty($message)) {
            return;
        }

        $err_id = crc32($message);
        if (! isset($this->content[ $err_id ])) {
            $this->content[ $err_id ] = new DebugLog();
            $this->content[ $err_id ]->id = $err_id;
            $this->content[ $err_id ]->count = 1;
        } else {
            $this->content[ $err_id ]->count++;
        }

        $date_create = date_create($arr[1]); // false if no valid date.
        $this->content[ $err_id ]->time = $date_create ? $date_create->format( DateTimeInterface::ATOM) : $arr[1];
        $this->content[ $err_id ]->message = $message;
        $this->content[ $err_id ]->type = implode( ' ', array_slice( str_word_count($message, 2), 1, 2 ) );
    }

    /**
     * Template table.
     *
     * @param string $content Content.
     *
     * @return string
     */
    private function templateTable(string $content): string {
        return sprintf( '<table class="wp-list-table widefat striped table-view-list">%s</table>', $content);
    }

    /**
     * Template table body.
     *
     * @return string
     */
    private function templateBody(): string {
        return sprintf( '<tbody><tr>%s</tr></tbody>', implode( '', array_map(fn($row) => '<tr>' . $this->templateColumn($row) . '</tr>', $this->getContents() ?? array())));
    }

    /**
     * Template.
     *
     * @param DebugLog $row Debug log.
     *
     * @return string
     */
    private function templateColumn(DebugLog $row): string {
        $map_cols = array_map( fn($col) => sprintf( '<td>%s</td>', $row->{$col}), $this->cols);
        return implode( '', $map_cols);
    }

    /**
     * Template table head.
     *
     * @return string
     */
    private function templateHead(): string {
        $this->cols = array_keys(get_class_vars('Inc\Ext\Components\DebugLogs\DebugLog'));
        return sprintf( '<thead>%s</thead>', implode( '', array_map( fn($col ) => sprintf( '<th>%s</th>', $col ), $this->cols ) ) );
    }
}
