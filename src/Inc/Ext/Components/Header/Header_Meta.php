<?php

namespace Inc\Ext\Components\Header;

use Inc\Ext\Utils\Text;


/**
 * Header meta.
 */
class Header_Meta {
    /**
     * @param array|null $colors
     */
    public function __construct(public ?array $colors = null) {
        $this->colors = $this->colors ?? ['#5bbad5', '"#ffffff', '"#ffffff'];

		add_action( 'wp_head', array($this, 'viewport_meta') , '1' );
		add_action( 'wp_head', array($this, 'favicon') , '1' );
		add_action( 'wp_head', array($this, 'nonce') , '1' );
		add_action( 'wp_head', array($this, 'template_version') , '1' );
        // Login head
		add_action( 'login_head', array($this, 'favicon') , '1' );
	}

	/**
     * Add viewport.
     *
	 * @return void
	 */
	public function viewport_meta(): void {
		echo '<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />';
	}

	/**
     * Generate nonce.
     *
	 * @return void
	 */
	public function nonce(): void {
		echo sprintf( "<meta name=\"wp-nonce\" content=\"%s\" />", wp_create_nonce( 'cockpit' ) );
	}

    /**
     * Add favicon.
     *
     * @return void
     */
	public function favicon(): void {
        $path = get_stylesheet_directory_uri();
        ob_start();
        ?>
        <link rel="icon" type="image/x-icon" href="<?php echo $path ?>/assets/favicon/favicon.ico">
        <link rel="apple-touch-icon" sizes="180x180" href="<?php echo $path ?>/assets/favicon/apple-touch-icon.png" />
        <link rel="icon" type="image/png" sizes="32x32" href="<?php echo $path ?>/assets/favicon/favicon-32x32.png" />
        <link rel="icon" type="image/png" sizes="16x16" href="<?php echo $path ?>/assets/favicon/favicon-16x16.png" />
        <link rel="manifest" href="<?php echo $path ?>/assets/favicon/site.webmanifest" />
        <link rel="mask-icon" href="<?php echo $path ?>/assets/favicon/safari-pinned-tab.svg" color="<?php echo esc_html( $this->colors[0] ) ?>" />
        <meta name="msapplication-TileColor" content="<?php echo esc_html( $this->colors[1] ) ?>" />
        <meta name="theme-color" content="<?php echo esc_html( $this->colors[2] ) ?>" />
        <?php
        echo Text::remove_spaces( ob_get_clean(), 2 );
	}

	/**
     * Add template version.
     *
	 * @return void
	 */
	public function template_version(): void {
	}
}
