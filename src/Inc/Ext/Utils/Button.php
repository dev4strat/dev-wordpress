<?php

namespace Inc\Ext\Utils;

use Inc\Ext\Utils\Traits\EscapeTrait;

class Button
{
    use EscapeTrait;

    /**
     * Constructor.
     *
     * @param string|null $title Title.
     * @param string|null $buttonClass
     * @param string|null $url
     * @param string|null $content
     * @param string|null $icon
     * @param string|null $attribute
     */
    public function __construct(
        private ?string $title = null,
        private ?string $buttonClass = null,
        private ?string $url = null,
        private ?string $content = null,
        private ?string $icon = null,
        private ?string $attribute = null,
    ) {
    }

    /**
     * @return string|null
     */
    public function getTitle(): ?string
    {
        return $this->title;
    }

    /**
     * @param string|null $title
     * @return Button
     */
    public function setTitle(?string $title): Button
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getButtonClass(): ?string
    {
        return 'btn' . ($this->buttonClass ? ' ' . $this->buttonClass : '');
    }

    /**
     * @param string|null $buttonClass
     * @return Button
     */
    public function setButtonClass(?string $buttonClass): Button
    {
        $this->buttonClass = $buttonClass;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getUrl(): ?string
    {
        return $this->url;
    }

    /**
     * @param string|null $url
     * @return Button
     */
    public function setUrl(?string $url): Button
    {
        $this->url = esc_url($url);
        return $this;
    }

    /**
     * @return string|null
     */
    public function getContent(): ?string
    {
        return $this->title ?? $this->content ?? '';
    }

    /**
     * @param string|null $content
     * @return Button
     */
    public function setContent(?string $content): Button
    {
        $this->content = $content;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getIcon(): ?string
    {
        return $this->icon;
    }

    /**
     * @param string|null $icon
     * @return Button
     */
    public function setIcon(?string $icon): Button
    {
        $this->icon = $icon;
        return $this;
    }

    /**
     * @return string
     */
    public function getAttribute(): string
    {
        return $this->attribute ? ' ' . $this->attribute : '';
    }

    /**
     * @param string|null $attribute
     * @return Button
     */
    public function setAttribute(?string $attribute): Button
    {
        $this->attribute = $attribute;
        return $this;
    }

    /**
     * Button primary style..
     *
     * @return $this
     */
    public function submit(): static {
        $this->value = sprintf(
            '<button type="submit" class="%1$s" title="%4$s"%3$s>%2$s</button>',
            $this->getButtonClass(),
            $this->getContent(),
            $this->getAttribute(),
            $this->getTitle() ?? '',
        );
        return $this;
    }

    /**
     * Link.
     * @return $this
     */
    public function href(): static {
        $this->value = sprintf(
            '<a href="%1$s" %4$s title="%2$s"%5$s>%3$s</a>',
            $this->getUrl(),
            $this->getTitle() ?? '',
            $this->getContent(),
            $this->buttonClass ? sprintf( 'class="%s"', $this->buttonClass ) : '',
            $this->getAttribute() ? ' ' . $this->getAttribute() : '',
        );
        return $this;
    }
}
