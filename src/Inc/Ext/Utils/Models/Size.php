<?php
/**
 * Size model.
 */

namespace Inc\Ext\Utils\Models;

/**
 * Model Size for like image size.
 */
class Size
{
    /**
     * Constructor.
     *
     * @param int|null $width Width.
     * @param int|null $height Height.
     */
    public function __construct(
        public ?int $width = null,
        public ?int $height = null
    )
    {
    }
}
