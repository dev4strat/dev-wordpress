<?php
/**
 * Field type for creating HTML field.
 *
 * @package dev4strat
 * @since   2024
 */

namespace Inc\Ext\Utils\Enums;

/**
 * Enum FieldType
 */
enum FieldType {
	case EMAIL;
	case HIDDEN;
	case NUMBER;
	case PASSWORT;
	case RADIO;
	case TEL;
	case TEXT;
	case TEXTAREA;
	case URL;
}
