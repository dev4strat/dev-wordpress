<?php
/**
 * Create new field.
 *
 * @package dev4strat
 * @since   2024
 */

namespace Inc\Ext\Utils;

/**
 * Sanitize text.
 */
class Text {
	/**
	 * Remove spaces of text.
	 * Use to trim for ob_start and ob_get_clean.
	 *
	 * @param string $text Text.
	 * @param int $space_length Length of spaces.
	 *
	 * @return string
	 */
	public static function remove_spaces( string $text, int $space_length = 1 ): string {
		$spaces = str_repeat( ' ', $space_length );
		return str_replace( $spaces, '', $text );
	}

    /**
     * Remove spaces outside of HTML tag.
     * Use to trim for ob_start and ob_get_clean.
     *
     * @param string $text Text.
     * @param int $space_length Length of spaces.
     *
     * @return string
     */
    public static function remove_spaces_outside_tag( string $text): string {
        return self::remove_spaces($text, 2);
    }

    /**
     * Remove spaces outside of HTML tag.
     * Use to trim for ob_start and ob_get_clean.
     *
     * @param string $text Text.
     * @param int $space_length Length of spaces.
     *
     * @return string
     */
    public static function remove_all_spaces_outside_tag( string $text): string {
        return trim(self::remove_spaces_outside_tag($text));
    }
}
