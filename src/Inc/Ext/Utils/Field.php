<?php
/**
 * Create new field.
 *
 * @package dev4strat
 * @since   2024
 */

namespace Inc\Ext\Utils;

use Inc\Ext\Utils\Enums\FieldType;
use Inc\Ext\Utils\Traits\EscapeTrait;

/**
 * Create html field (input tag or textarea tag) for form.
 * @uses FieldType
 *
 * @example return string HTML input tag: ( new Field($fieldName, null, $title) )->label()->getValue()
 * @example return void HTML input tag: ( new Field($fieldName, FieldType::EMAIL, $title, $placeholder, $description) )->label()->html()
 * @link classes/FieldTest.html
 */
class Field
{
    use EscapeTrait;

    /**
     * Set parameters for creating field (input or textarea).
     * FieldType::TEXT is default for $fieldType.
     *
     * @param string $fieldName Field name for label and field.
     * @param FieldType|null $fieldType Field type.
     * @param string|null $title Title for label
     * @param string|null $placeholder Placeholder in field.
     * @param string|null $description Description under field.
     */
    public function __construct(
        private readonly string    $fieldName,
        private ?FieldType $fieldType = null,
        private readonly ?string   $title = null,
        private readonly ?string   $placeholder=null,
        private readonly ?string   $description=null
    )
    {
        $this->fieldType = $this->fieldType ?? FieldType::TEXT;
    }


    /**
     * Get placeholder.
     *
     * @return string
     */
    private function getPlaceholder(): string
    {
        return ($this->placeholder ?? '');
    }


    /**
     * Get label.
     *
     * @return $this
     */
    public function label(): static
    {
        $this->value = sprintf('<label for="%s" class="form-label">%s</label>', $this->fieldName, $this->title);
        return $this;
    }


    /**
     * Generate Field (input or textarea).
     *
     * @return $this
     */
    public function field(): static
    {
        $this->value = match ($this->fieldType) {
            FieldType::TEXTAREA => $this->textArea(),
            default => $this->inputString(),
        };

        $this->value = $this->value. $this->errorField() . $this->getDescription();
        return $this;
    }


    /**
     * Error field.
     *
     * @return string
     */
    private function errorField(): string
    {
        return sprintf('<div id="%s-error" class="invalid-feedback"></div>', $this->fieldName);
    }


    /**
     * Generate description under field.
     *
     * @return string
     */
    private function getDescription(): string
    {
        return $this->description ? sprintf('<div class="form-text">%s</div>', $this->description) : '';
    }


    /**
     * Create Input with different fieldType.
     *
     * @return string
     */
    private function inputString(): string
    {
        return sprintf(
            /** @lang text */            '<input type="%s" name="%s" placeholder="%s" class="form-control" id="%s" />',
            strtolower($this->fieldType->name),
            $this->fieldName,
            $this->getPlaceholder(),
            $this->fieldName
        );
    }


    /**
     * Create Textarea.
     *
     * @return string
     */
    private function textArea(): string
    {
        return sprintf(
            '<textarea name="%1$s" placeholder="%2$s" class="form-control" id="%1$s"></textarea>',
            $this->fieldName,
            $this->getPlaceholder()
        );
    }
}
