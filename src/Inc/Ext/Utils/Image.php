<?php
/**
 * Create new field.
 *
 * @package dev4strat
 * @since   2024
 */

namespace Inc\Ext\Utils;

use Exception;
use Inc\Ext\Utils\Models\Size;
use Inc\Ext\Utils\Traits\EscapeTrait;

/**
 * Create Html image with source, alt and class attribute.
 * @uses get_stylesheet_directory_uri for theme uri or defined ASSETS_DIR.
 */
class Image
{
    use EscapeTrait;

    /**
     * Folder.
     * @var string
     */
    private string $folder = '/assets/images/';

    private ?string $class_name = null;
    private ?Size $size = null;

    /**
     * Image Constructor.
     *
     * @param string $filename Filename without source.
     * @param string|null $image_title Title of Image.
     */
    public function __construct(
        public string $filename,
        private readonly ?string $image_title = null
    )
    {
    }

    /**
     * Get Image title.
     *
     * @return string
     */
    public function get_title(): string
    {
        return $this->image_title ?? get_bloginfo('name');
    }

    /**
     * @return string
     */
    public function get_path_filename(): string {
        return $this->path() . $this->filename;
    }

    /**
     * @return string
     */
    public function get_uri_filename(): string {
        return $this->uri() . $this->filename;
    }

    /**
     * @return bool
     */
    private function has_file(): bool {
        return file_exists($this->get_path_filename());
    }

    /**
     * @return string
     */
    private function get_path_webp(): string {
        $info = pathinfo($this->path() . $this->filename);
        $path = $info['dirname'];
        $name = $info['filename'];

        return $path .'/'. $name . '.webp';
    }

    /**
     * @return string
     */
    private function get_uri_webp(): string {
        $info = pathinfo($this->uri() . $this->filename);
        $path = $info['dirname'];
        $name = $info['filename'];

        return $path .'/'. $name . '.webp';
    }

    /**
     * @return bool
     */
    private function has_webp(): bool {
        return file_exists($this->get_path_webp());
    }

    /**
     * Set html attribute class.
     *
     * @param string|null $value
     *
     * @return $this
     */
    public function setClass( ?string $value = null ): static
    {
        $this->class_name = $value;
        return $this;
    }

    /**
     * Set attribute Size.
     * @param Size|null $value
     *
     * @return $this
     */
    public function setSize( ?Size $value = null ): static
    {
        $this->size = $value;
        return $this;
    }

    /**
     * Get Image file in /assets/images/ from theme direction.
     *
     * @throws Exception
     */
    public function asset(): static
    {
        if ($this->has_webp()) {
            $this->value = $this->asset_picture_html();
            return $this;
        }

        if ( $this->has_file() ) {
            $this->value = $this->asset_image_html();
            return $this;
        }

        throw new Exception("No found image file in {$this->path()}{$this->filename}.");
    }

    /**
     * @return string
     * @throws Exception
     */
    private function asset_image_html(): string {
        return sprintf(
            '<img src="%1$s" class="%2$s" alt="%3$s" title="%3$s" %4$s />',
            $this->get_uri_filename(),
            trim('img-fluid ' . ( $this->class_name ?? '' )),
            $this->get_title(),
            $this->size()
        );
    }

    /**
     * @return string
     * @throws Exception
     */
    private function asset_picture_html(): string {
        return sprintf(
            '<picture>
              <source srcset="%6$s" type="%7$s">
              <source srcset="%1$s" type="%5$s">
              <img src="%1$s" class="%2$s" alt="%3$s" title="%3$s" %4$s />
            </picture>',
            $this->get_uri_filename(),
            trim('img-fluid ' . ( $this->class_name ?? '' )),
            $this->get_title(),
            $this->size(),
            $this->get_image_type($this->get_uri_filename()),
            $this->get_uri_webp(),
            $this->get_image_type( $this->get_path_webp())
        );
    }

    /**
     * @param $filename
     *
     * @return string
     */
    private function get_image_type($filename): string {
        $info = pathinfo($filename);
        $extension = $info['extension'];
        return 'image/' . $extension;
    }

    /**
     * Image path.
     *
     *  @uses get_stylesheet_directory for theme uri or defined ASSETS_DIR.
     * @return string
     */
    private function path(): string
    {
        return ( function_exists('get_stylesheet_directory') ? get_stylesheet_directory() : ASSETS_DIR ) . $this->folder;
    }

    /**
     * Image Uri.
     *
     * @uses get_stylesheet_directory_uri for theme uri or defined ASSETS_DIR.
     * @return string
     */
    private function uri(): string
    {
        return ( function_exists('get_stylesheet_directory_uri') ? get_stylesheet_directory_uri() : ASSETS_DIR ) . $this->folder;
    }

    /**
     * Image size.
     *
     * @throws Exception
     * @return string
     */
    private function size(): string
    {
        if (!$this->size) {
            return '';
        }

        if ($this->size->width && $this->size->height) {
            return sprintf('witdh="%1$s" height="%2$s"', $this->size->width, $this->size->height);
        }

        if ($this->size->width) {
            return sprintf('witdh="%s"', $this->size->width);
        }

        if ($this->size->height) {
            return sprintf('height="%s"', $this->size->height);
        }

        throw new Exception("No found image Size in {$this->path()}.");
    }
}
