<?php
/**
 * Route trait with methods.
 *
 * @package dev4strat
 * @since   2024
 */

namespace Inc\Ext\Utils\Traits;

use WP_REST_Response;

/**
 * Trait Route.
 */
trait RouteTrait {
    /**
     * Get method.
     *
     * @return WP_REST_Response|null
     */
    public function get(): ?WP_REST_Response {
        return rest_ensure_response( array() );
    }

    /**
     * Post method.
     *
     * @return WP_REST_Response|null
     */
    public function post(): ?WP_REST_Response {
        return rest_ensure_response( array() );
    }

    /**
     * Delete method.
     *
     * @return WP_REST_Response|null
     */
    public function delete(): ?WP_REST_Response {
        return rest_ensure_response( array() );
    }
}
