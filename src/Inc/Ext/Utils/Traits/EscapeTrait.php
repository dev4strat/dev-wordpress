<?php
/**
 * Escape text for echo wordpress.
 *
 * @package dev4strat
 * @since   2024
 */

namespace Inc\Ext\Utils\Traits;

/**
 * Escape trait.
 */
trait EscapeTrait {
	/**
	 * Value.
	 *
	 * @var string
	 */
	private string $value = '';

	/**
	 * Show string.
	 * @return void
	 */
	public function string(): void {
		echo esc_textarea( $this->value );
	}

	/**
	 * Show string.
	 * @return void
	 */
	public function html(): void {
		$allowed_html = array_merge( wp_kses_allowed_html( 'post' ), $this->allowedTags() );
		echo wp_kses( $this->value, $allowed_html );
	}


    /**
     * Return field.
     *
     * @return string
     */
    public function getValue(): string {
        return $this->value;
    }

    /**
     *  Return all fields as json format.
     *
     * @return array
     */
    public function toJson(): array {
        return get_object_vars( $this );
    }

	/**
	 * Allowed HTML tags.
	 * @return array[]
	 */
	private function allowedTags(): array {
		return array(
            'a' => array(
                'href' => true,
                'title' => true,
                'class' => true,
                'target' => true,
                'download' => true,
                'data-bs-toggle' => true,
                'data-bs-target' => true,
                'aria-expanded' => true,
                'aria-controls' => true,
                'role' => true,
            ),
			'label'    => array(
				'for'   => array(),
				'class' => array(),
			),
			'input'    => array(
				'id'          => array(),
				'value'       => array(),
				'class'       => array(),
				'type'        => array(),
				'disabled'    => array(),
				'name'        => array(),
				'placeholder' => array(),
			),
			'textarea' => array(
				'id'          => array(),
				'class'       => array(),
				'readonly'    => array(),
				'rows'        => array(),
				'name'        => array(),
				'placeholder' => array(),
			),
            'picture' => array(),
            'source' => array(
                'srcset' => true,
                'type' => true,
                'src' => true,
            ),
            'img' => array(
                'src' => true,
                'srcset' => true,
                'sizes' => true,
                'class' => true,
                'id' => true,
                'width' => true,
                'height' => true,
                'alt' => true,
                'title' => true,
                'loading' => true
            ),
		);
	}
}
