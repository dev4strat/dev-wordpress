<?php
/**
 * Routes
 *
 * @package dev4strat
 * @since 2024
 */

namespace Inc\Ext\Utils\Abstracts;

/**
 * class Base routes.
 */
class RoutesController {
    /**
     * Register routes.
     * Change route namespace.
     */
    public function __construct() {
        // Change wp-json to api.
        add_filter( 'rest_url_prefix', fn() => 'api' );
        add_action( 'rest_api_init', array( $this, 'register' ) );
    }

    /**
     * Register all routes.
     *
     * @return void
     */
    public function register(): void {}
}
