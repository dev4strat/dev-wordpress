<?php
/**
 * Register route abstract.
 *
 * @package dev4strat
 * @since 2024
 */

namespace Inc\Ext\Utils\Abstracts;

/**
 * Abstract Route controller.
 */
abstract class RouteController {
    /**
     * Route register.
     *
     * @return array
     */
    abstract public function register(): array;
}
