<?php
/**
 * Dev helper for composer.
 *
 * @package devWordpress
 * @since   2024
 */

namespace Dev4strat\DevWordpress;

use FilesystemIterator;
use RecursiveIteratorIterator;
use Illuminate\Filesystem\Filesystem;

class DevHelper
{
    /**
     * Print line.
     *
     * @param string|null $string $string Text.
     * @return void
     */
    public static function println( ?string $string ): void
    {
        echo "\033[96m$string" . PHP_EOL;
    }


    /**
     * Copy folder with all files.
     *
     * @param string $src Source.
     * @param string $dest Destination.
     * @return void
     */
    public static function copyFolder( string $src, string $dest ): void
    {
        $filesystem = new Filesystem();

        // Stelle sicher, dass das Zielverzeichnis existiert
        $filesystem->ensureDirectoryExists($dest, 0755);

        // Dateien aus dem Quellverzeichnis abrufen (anstelle von $dest)
        $files = $filesystem->allFiles($src, true);

        foreach ($files as $item) {
            // Dateiendung abrufen
            $extension = $item->getExtension();
            var_dump('Copy: ' . $item->getRealPath());

            // Bestimmte Dateitypen ausschließen
            if (in_array($extension, ['ts', 'tsx', 'scss'])) {
                continue;
            }

            $relative_path = $item->getRelativePathname();
            $destination_path = $dest . DIRECTORY_SEPARATOR . $relative_path;

            // Stelle sicher, dass das Zielverzeichnis existiert
            $filesystem->ensureDirectoryExists(dirname($destination_path), 0755);

            // Datei kopieren
            $filesystem->copy($item->getRealPath(), $destination_path);
        }
    }

    /**
     * Remove all folders and files.
     *
     * @param string $dir Path.
     * @param bool   $sub Remove only subfolder.
     * @return void
     */
    public static function remove_all( string $dir, bool $sub = true ): void
    {
        if ( is_dir( $dir ) ) {
            $objects = scandir( $dir );
            foreach ( $objects as $object ) {
                if ( $object != "." && $object != ".." ) {
                    if ( filetype( $dir . "/" . $object ) == "dir" ) {
                        DevHelper::remove_all( $dir . "/" . $object, true );
                    } else {
                        unlink( $dir . "/" . $object );
                    }
                }
            }
            reset( $objects );

            if ( $sub ) {
                rmdir( $dir );
            }
        }
    }

    /**
     * Remove all files from folder.
     *
     * @param string $dir Path.
     * @return void
     */
    public static function remove_files_from_folder( string $dir ): void
    {
        DevHelper::remove_all( $dir, false );
    }

    /**
     * Get files and folders.
     *
     * @param string $src Source.
     * @return RecursiveIteratorIterator
     */
    public static function get_files( string $src ): RecursiveIteratorIterator
    {
        $directoryIterator = new \RecursiveDirectoryIterator( $src, FilesystemIterator::SKIP_DOTS );
        return new \RecursiveIteratorIterator( $directoryIterator, \RecursiveIteratorIterator::SELF_FIRST );
    }
}
