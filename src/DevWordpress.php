<?php
/**
 * Initialize wordpress with composer.
 *
 * @package devWordpress
 * @since   2024
 */

namespace Dev4strat\DevWordpress;

use Composer\Script\Event;
use Symfony\Component\Filesystem\Filesystem;

// phpcs:disable
class DevWordpress extends DevDocker
{
    public static function copyConfig(Event $event): void {
        parent::getRoot($event);
        self::copy(self::$project_path . '/app/config/local', self::$project_path . '/out');
    }

    public static function copyPlugins(Event $event): void {
        parent::getRoot($event);
        self::copy(self::$project_path . '/app/src/plugins', self::$project_path . '/out/wp-content/plugins');
    }

    public static function copyFavicons(): void {
        self::copy('./app/src/assets/favicons', './out/favicons');
    }

    public static function copyTheme(Event $event): void {
        parent::getRoot($event);
        parent::loadEnv();
        self::copy(self::$project_path . '/app/src/theme', self::$project_path . '/out/wp-content/themes/' . $_ENV[ 'THEME' ]);
    }

    public static function copyLanguages(Event $event): void {
        parent::getRoot($event);
        parent::loadEnv();
        self::copy(self::$project_path . '/app/src/languages', self::$project_path . '/out/wp-content/themes/' . $_ENV[ 'THEME' ] .'/languages');
    }

    public static function _copyFonts(string $src): void {
        parent::loadEnv();
        self::copy(self::$project_path . $src, self::$project_path . '/out/wp-content/themes/' . $_ENV[ 'THEME' ] .'/assets/fonts');
    }

    public static function createUploads(Event $event): void {
        parent::getRoot($event);
        (new FileSystem())->mkdir(self::$project_path . '/out/wp-content/uploads');
    }

    public static function removeUnusedThemes(Event $event): void {
        parent::getRoot($event);
        DevHelper::remove_files_from_folder(self::$project_path . '/out/wp-content/themes');
    }

    public static function loadConfig(): void {
        parent::loadEnv();
        self::console('php ./dev/Config.php --port=53' . $_ENV[ 'PORT_ID' ]);
    }

    public static function loadImages(): void {
        parent::loadEnv();
        $cmd = 'wp media import --path=' . self::$project_path . '/out ' . self::$project_path . $_ENV[ 'ASSET_IMAGES' ];
        self::docker($cmd);
    }

    public static function reloadImages(): void {
        self::console('php ./dev/WordpressCli.php');
    }

    public static function deleteImages(): void {
        self::console("wp post delete --force --path='" . self::$project_path . "/out' $(wp post list --post_type='attachment' --format=ids --path='" . self::$project_path . "/out')");
    }

    /**
     * @param Event $event Composer Event.
     *
     * @return void
     */
    public static function loadExtUtils(Event $event): void {
        parent::getRoot($event);
        parent::loadEnv();

        self::copy( self::$project_path . '/vendor/dev4strat/dev-wordpress/src/Inc/Ext', self::$project_path . '/out/wp-content/themes/' . $_ENV[ 'THEME' ] . '/Inc/Ext' );
    }
}
