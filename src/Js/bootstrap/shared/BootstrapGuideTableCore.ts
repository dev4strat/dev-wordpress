interface BootstrapGuideFormatItem {
  title: string;
  class: string;
}

export class BootstrapGuideTableCore {
  selector: HTMLElement;
  data: BootstrapGuideFormatItem[];
  columns: string[];

  constructor(selector: HTMLElement) {
    this.selector = selector;

    if (this.selector) {
      this.setData();
      this.init();
    }
  }

  setData() {}

  init() {
    this.selector.querySelector("table thead").innerHTML = this.templateHead();
    this.selector.querySelector("table tbody").innerHTML = this.data.map((r) => this.template(r)).join("");

    this.computedStyle();
  }

  private templateHead() {
    return `<tr>
      <th>Title</th>
      <th>class</th>
      ${this.columns.map((c) => `<th>${c}</th>`).join("")}
    </tr>`;
  }

  private template(r: BootstrapGuideFormatItem) {
    return `<tr>
      <td><span class="${r.class}">${r.title}</span></td>
      <td>${r.class}</td>

      ${this.columns.map((c) => `<td class="value" data-style="${c}"></td>`).join("")}
      </tr>`;
  }

  private computedStyle() {
    const rows = this.selector.querySelector("table tbody").querySelectorAll("tr");
    rows.forEach((r) => {
      const heading = r.querySelector("span");
      const cols = r.querySelectorAll(".value");
      const computedStyle: CSSStyleDeclaration = window.getComputedStyle(heading);

      cols.forEach((col) => {
        const key = col.getAttribute("data-style");

        if (key === "var") {
          col.innerHTML = this.getCSSVariableNamesByClass(heading.className);
        } else {
          // Extract the RGB values
          // @ts-ignore
          if (computedStyle[key].includes("rgb")) {
            // @ts-ignore
            const rgbValues = computedStyle[key].match(/\d+/g); // ["255", "0", "0"]
            // Convert RGB to Hex
            col.innerHTML = this.rgbToHex(rgbValues);
          } else {
            // @ts-ignore
            col.innerHTML = computedStyle[key];
          }
        }
      });
    });
  }

  // Function to get CSS variable names for a specific class
  getCSSVariableNamesByClass(className: string) {
    const variableNames = [];

    // Loop through all stylesheets
    for (const stylesheet of document.styleSheets) {
      try {
        // Loop through all CSS rules in the stylesheet
        for (const rule of stylesheet.cssRules) {
          // Check if the rule's selector text includes the class name
          // @ts-ignore
          if (rule.selectorText && rule.selectorText.includes(className)) {
            // Loop through all styles in the rule
            // @ts-ignore
            if (rule.cssText.includes(`.${className} {`)) {
              const styles = rule.cssText.replace(`.${className} {`, "").replace("}", "").split(";");
              variableNames.push(styles.join("<br>")); // Add variable name to the list
            }
          }
        }
      } catch (e) {
        // Catch and ignore CORS errors for external stylesheets
        console.warn(`Could not access stylesheet: ${stylesheet.href}`, e);
        variableNames.push("-");
      }
    }

    return variableNames.join("");
  }

  private rgbToHex(rgbValues: string[]) {
    const r = parseInt(rgbValues[0], 10);
    const g = parseInt(rgbValues[1], 10);
    const b = parseInt(rgbValues[2], 10);

    const toHex = (value: number) => {
      const hex = value.toString(16);
      return hex.length === 1 ? "0" + hex : hex;
    };

    return `#${toHex(r).toUpperCase()}${toHex(g).toUpperCase()}${toHex(b).toUpperCase()}`;
  }
}
