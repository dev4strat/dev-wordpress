import { BootstrapGuideTableCore } from "../shared/BootstrapGuideTableCore";

export class BootstrapGuideTextBackgrounds extends BootstrapGuideTableCore {
  constructor() {
    const selector = document.querySelector(".section-bs-text-backgrounds") as HTMLElement;

    super(selector);
  }

  setData() {
    this.data = [
      { title: "text-bg-primary", class: "text-bg-primary" },
      { title: "text-bg-secondary", class: "text-bg-secondary" },
      { title: "text-bg-success", class: "text-bg-success" },
      { title: "text-bg-danger", class: "text-bg-danger" },
      { title: "text-bg-warning", class: "text-bg-warning" },
      { title: "text-bg-info", class: "text-bg-info" },
      { title: "text-bg-light", class: "text-bg-light" },
      { title: "text-bg-dark", class: "text-bg-dark" },
    ];

    this.columns = ["backgroundColor", "var"];
  }
}
