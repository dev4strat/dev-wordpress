import { BootstrapGuideTableCore } from "../shared/BootstrapGuideTableCore";

export class BootstrapGuideBackgrounds extends BootstrapGuideTableCore {
  constructor() {
    const selector = document.querySelector(".section-bs-backgrounds") as HTMLElement;

    super(selector);
  }

  setData() {
    this.data = [
      { title: "bg-primary", class: "bg-primary" },
      { title: "bg-secondary", class: "bg-secondary" },
      { title: "bg-success", class: "bg-success" },
      { title: "bg-danger", class: "bg-danger" },
      { title: "bg-warning", class: "bg-warning" },
      { title: "bg-info", class: "bg-info" },
      { title: "bg-light", class: "bg-light" },
      { title: "bg-dark", class: "bg-dark" },
      { title: "bg-body", class: "bg-body" },
      { title: "bg-body-secondary", class: "bg-body-secondary" },
      { title: "bg-body-tertiary", class: "bg-body-tertiary" },
      { title: "bg-black", class: "bg-black" },
      { title: "bg-white", class: "bg-white" },
      { title: "bg-transparent", class: "bg-transparent" },
    ];

    this.columns = ["backgroundColor", "var"];
  }
}
