import { BootstrapGuideTableCore } from "../shared/BootstrapGuideTableCore";

export class BootstrapGuideWeights extends BootstrapGuideTableCore {
  constructor() {
    const selector = document.querySelector(".section-bs-font-weights") as HTMLElement;

    super(selector);
  }

  setData() {
    this.data = [
      { title: "extrabold", class: "fw-bolder" },
      { title: "bold", class: "fw-bold" },
      { title: "normal", class: "fw-normal" },
      { title: "thin", class: "fw-light" },
    ];

    this.columns = ["fontWeight"];
  }
}
