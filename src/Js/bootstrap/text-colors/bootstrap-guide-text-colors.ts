import { BootstrapGuideTableCore } from "../shared/BootstrapGuideTableCore";

export class BootstrapGuideTextColors extends BootstrapGuideTableCore {
  constructor() {
    const selector = document.querySelector(".section-bs-text-colors") as HTMLElement;

    super(selector);
  }

  setData() {
    this.data = [
      { title: "text-primary", class: "text-primary" },
      { title: "text-primary-emphasis", class: "text-primary-emphasis" },
      { title: "text-secondary", class: "text-secondary" },
      { title: "text-secondary-emphasis", class: "text-secondary-emphasis" },
      { title: "text-success", class: "text-success" },
      { title: "text-success-emphasis", class: "text-success-emphasis" },
      { title: "text-danger", class: "text-danger" },
      { title: "text-danger-emphasis", class: "text-danger-emphasis" },
      { title: "text-warning", class: "text-warning" },
      { title: "text-warning-emphasis", class: "text-warning-emphasis" },
      { title: "text-info", class: "text-info" },
      { title: "text-info-emphasis", class: "text-info-emphasis" },
      { title: "text-light", class: "text-light" },
      { title: "text-light-emphasis", class: "text-light-emphasis" },
      { title: "text-dark", class: "text-dark" },
      { title: "text-dark-emphasis", class: "text-dark-emphasis" },
      { title: "text-body", class: "text-body" },
      { title: "text-body-emphasis", class: "text-body-emphasis" },
      { title: "text-body-secondary", class: "text-body-secondary" },
      { title: "text-body-tertiary", class: "text-body-tertiary" },
      { title: "text-black-50", class: "text-black-50" },
      { title: "text-white-50", class: "text-white-50" },
    ];

    this.columns = ["color", "fontSize"];
  }
}
