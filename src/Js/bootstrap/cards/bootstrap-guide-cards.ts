import { BootstrapGuideTableCore } from "../shared/BootstrapGuideTableCore";

export class BootstrapGuideCards extends BootstrapGuideTableCore {
  constructor() {
    const selector = document.querySelector(".section-bs-cards") as HTMLElement;

    super(selector);
  }

  setData() {
    this.data = [
      { title: "card", class: "card" },
      { title: "card-title", class: "card-title" },
      { title: "card-body", class: "card-body" },
    ];

    this.columns = ["var", "borderWidth", "borderColor"];
  }
}
