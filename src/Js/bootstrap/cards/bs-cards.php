<?php

use Inc\Shared\Models\SectionPart;

/**
 * Section part.
 *
 * @var SectionPart $section Section.
 */
$section = $args['section'] ?? null;
?>
<section class="<?php $section->section_class()->html(); ?>">
    <h2>Cards</h2>
    url: <a href='https://getbootstrap.com/docs/5.3/components/card/#about' target='_blank'>get bootstrap</a>
    <table class="table table-striped">
        <thead></thead>
        <tbody></tbody>
    </table>
</section>
