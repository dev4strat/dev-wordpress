import {BootstrapGuideFormats} from './formats/bootstrap-guide-formats';
import {BootstrapGuideWeights} from './weights/bootstrap-guide-weights';
import {BootstrapGuideTextColors} from './text-colors/bootstrap-guide-text-colors';
import {BootstrapGuideBackgrounds} from './backgrounds/bootstrap-guide-backgrounds';
import {BootstrapGuideTextBackgrounds} from './text-backgrounds/bootstrap-guide-text-backgrounds';
import {BootstrapGuideCards} from './cards/bootstrap-guide-cards';
import {BootstrapGuideSpacings} from './spacings/bootstrap-guide-spacings';

interface BootstrapGuideItem {
  title: string;
  class: string;
  url: string;
}

export class BootstrapGuide {
  selector: HTMLElement;

  constructor() {
  }

  onInit() {
    this.selector = this.selector ?? document.querySelector('.bootstrap-guide');

    if (this.selector) {
      this.styleCSS();
      this.loadList();
      this.loadClasses();
    }
  }

  private loadList() {
    const data: BootstrapGuideItem[] = [
      {title: 'Font weights and CSS variables', class: 'section-bs-font-weights', url: 'https://getbootstrap.com/docs/5.3/utilities/text/#font-weight-and-italics'},
      {title: 'Font formats for user interfaces', class: 'section-bs-font-formats', url: 'https://getbootstrap.com/docs/5.3/utilities/text/#font-weight-and-italics'},
      {title: 'Text Colors', class: 'section-bs-text-colors', url: 'https://getbootstrap.com/docs/5.3/utilities/colors/'},
      {title: 'Text Backgrounds', class: 'section-bs-text-backgrounds', url: 'https://getbootstrap.com/docs/5.3/helpers/color-background/'},
      {title: 'Backgrounds', class: 'section-bs-backgrounds', url: 'https://getbootstrap.com/docs/5.3/utilities/background/'},
      {title: 'Cards', class: 'section-bs-cards', url: 'https://getbootstrap.com/docs/5.3/components/card/#about'},
      {title: 'Spacings', class: 'section-bs-spacings', url: 'https://getbootstrap.com/docs/5.3/utilities/spacing/#sass-maps'},
    ]

    this.selector.innerHTML = `<div class="vstack gap-5">${data.map(this.template).join('')}</div>`;
  }

  template(item: BootstrapGuideItem) {
    return `<section class="${item.class}">
        <h2>${item.title}</h2>
        <div class="${item.url ? '' : 'd-none'}">Url: <a href="${item.url}" target="_blank">Get bootstrap</a></div>
        <table class="table table-striped"><thead></thead><tbody></tbody></table>
    </section>`;
  }

  loadClasses() {
    new BootstrapGuideWeights();
    new BootstrapGuideFormats();
    new BootstrapGuideTextColors();
    new BootstrapGuideBackgrounds();
    new BootstrapGuideTextBackgrounds();
    new BootstrapGuideCards();
    new BootstrapGuideSpacings();
  }

  styleCSS() {
    const compiledCSS = `
      .section-bs-backgrounds,
      .section-bs-text-backgrounds {
        span {
          display: block;
          height: 6rem;
          width: 20rem;
        }
      }
  `;

    const style = document.createElement('style');
    style.type = 'text/css';
    style.appendChild(document.createTextNode(compiledCSS));
    document.head.appendChild(style);
  }
}
