import { BootstrapGuideTableCore } from "../shared/BootstrapGuideTableCore";

export class BootstrapGuideFormats extends BootstrapGuideTableCore {
  constructor() {
    const selector = document.querySelector(".section-bs-font-formats") as HTMLElement;

    super(selector);
  }

  setData() {
    this.data = [
      { title: "Heading 1", class: "h1" },
      { title: "Heading 2", class: "h2" },
      { title: "Heading 3", class: "h3" },
      { title: "Heading 4", class: "h4" },
      { title: "Heading 5", class: "h5" },
      { title: "Heading 6", class: "h6" },
      { title: "fs-1", class: "fs-1" },
      { title: "fs-2", class: "fs-2" },
      { title: "fs-3", class: "fs-3" },
      { title: "fs-4", class: "fs-4" },
      { title: "fs-5", class: "fs-5" },
      { title: "fs-6", class: "fs-6" },
      { title: "body", class: "body" },
      { title: "small", class: "small" },
      { title: "title-1", class: "title-1" },
    ];

    this.columns = ["fontSize", "lineHeight", "fontWeight", "fontFamily", "letterSpacing"];
  }
}
