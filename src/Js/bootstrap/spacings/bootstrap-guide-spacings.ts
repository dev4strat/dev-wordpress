import { BootstrapGuideTableCore } from "../shared/BootstrapGuideTableCore";

export class BootstrapGuideSpacings extends BootstrapGuideTableCore {
  constructor() {
    const selector = document.querySelector(".section-bs-spacings") as HTMLElement;

    super(selector);
  }

  setData() {
    this.data = [
      { title: "gap-0", class: "gap-0" },
      { title: "gap-1", class: "gap-1" },
      { title: "gap-2", class: "gap-2" },
      { title: "gap-3", class: "gap-3" },
      { title: "gap-4", class: "gap-4" },
      { title: "gap-5", class: "gap-5" },
    ];

    this.columns = ["var"];
  }
}
