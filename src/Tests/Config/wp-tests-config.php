<?php
/**
 * Testing configuration file with constants.
 *
 * @package dev4stratTests
 * @since   2024
 */

require_once __DIR__ . '/../../../vendor/autoload.php';

const WP_TESTS_TABLE_PREFIX     = 'sp_';
const IP                        = '127.0.0.1';
const WP_PHP_BINARY             = 'php';
const WP_TESTS_EMAIL            = 'admin@test.com';
const WP_UNIT_DIR               = __DIR__ . '/../../../vendor/lipemat/wp-unit';
const WP_TESTS_CONFIG_FILE_PATH = __FILE__;
// If your tests must use `https` URL.
const WP_TESTS_SSL = false;

// Root of your site/
define( 'WP_TESTS_DIR', dirname( __DIR__ ) . '/../Tests' );
define( 'ASSETS_DIR', dirname( __DIR__ ) . '/../Tests' );
define( 'ABSPATH', dirname( __DIR__ ) . '/../../../out/' );
