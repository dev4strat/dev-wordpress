<?php
/**
 * Bootstrap configuration for testing.
 *
 * @package dev4stratTests
 * @since   2024
 */

require __DIR__ . '/wp-tests-config.php';
require __DIR__ . '/../../../vendor/autoload.php';
