<?php
/**
 * Testing Field class.
 *
 * @package dev4stratTests
 * @since   2024
 */

use Inc\Ext\Utils\Enums\FieldType;
use Inc\Ext\Utils\Field;
use PHPUnit\Framework\TestCase;

/**
 * Field Test
 * @example ( new Field($fieldName, FieldType::EMAIL, $title, $placeholder, $description) )->label()->html()
 */
class FieldTest extends TestCase
{

    private string $fieldName = 'test';

    private string $errorField = '<div id="test-error" class="invalid-feedback"></div>';

    public function testCanBeCreatedLabel(): void
    {
        $title = 'test';
        $input = sprintf("<label for=\"%s\" class=\"form-label\">%s</label>", $this->fieldName, $title);
        static::assertSame($input, ( new Field($this->fieldName, null, $title))->label()->getValue());
    }

    public function testCanBeCreatedInputString(): void
    {
        $input = '<input type="text" name="'.$this->fieldName.'" placeholder="" class="form-control" id="'.$this->fieldName.'">' . $this->errorField;
        static::assertSame($input, ( new Field($this->fieldName, null, null))->field()->getValue());
    }

    public function testCanBeCreatedInputPlaceholder(): void
    {
        $placeholder = 'Bitte geben Sie einen Text ein.';
        $input = '<input type="text" name="'.$this->fieldName.'" placeholder="' . $placeholder. '" class="form-control" id="'.$this->fieldName.'">' . $this->errorField;
        static::assertSame($input, ( new Field($this->fieldName, null, null, $placeholder) )->field()->getValue());
    }

    public function testCanBeCreatedInputDescription(): void
    {
        $placeholder = 'Bitte geben Sie einen Text ein.';
        $descriptionField = sprintf('<div class="form-text">%s</div>', $placeholder);
        $input = '<input type="text" name="'.$this->fieldName.'" placeholder="' . $placeholder. '" class="form-control" id="'.$this->fieldName.'">' . $this->errorField . $descriptionField;
        static::assertSame($input, ( new Field($this->fieldName, null, null, $placeholder, $placeholder) )->field()->getValue());
    }

    public function testCanBeCreatedInputEmail(): void
    {
        $input = '<input type="email" name="'.$this->fieldName.'" placeholder="" class="form-control" id="'.$this->fieldName.'">' . $this->errorField;
        static::assertSame($input, ( new Field($this->fieldName, FieldType::EMAIL, null))->field()->getValue());
    }

    public function testCanBeCreatedInputPhone(): void
    {
        $input = '<input type="tel" name="'.$this->fieldName.'" placeholder="" class="form-control" id="'.$this->fieldName.'">' . $this->errorField;
        static::assertSame($input, ( new Field($this->fieldName, FieldType::TEL, null))->field()->getValue());
    }

    public function testCanBeCreatedInputHidden(): void
    {
        $input = '<input type="hidden" name="'.$this->fieldName.'" placeholder="" class="form-control" id="'.$this->fieldName.'">' . $this->errorField;
        static::assertSame($input, ( new Field($this->fieldName, FieldType::HIDDEN, null))->field()->getValue());
    }

    public function testCanBeCreatedInputNumber(): void
    {
        $input = '<input type="number" name="'.$this->fieldName.'" placeholder="" class="form-control" id="'.$this->fieldName.'">' . $this->errorField;
        static::assertSame($input, ( new Field($this->fieldName, FieldType::NUMBER, null))->field()->getValue());
    }

    public function testCanBeCreatedInputUrl(): void
    {
        $input = '<input type="url" name="'.$this->fieldName.'" placeholder="" class="form-control" id="'.$this->fieldName.'">' . $this->errorField;
        static::assertSame($input, ( new Field($this->fieldName, FieldType::URL, null))->field()->getValue());
    }
}
