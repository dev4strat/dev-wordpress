<?php
/**
 * Testing Image class.
 *
 * @package dev4stratTests
 * @since   2024
 */

use Inc\Ext\Utils\Image;
use PHPUnit\Framework\TestCase;

/**
 * Test Image.
 */
class ImageTest extends TestCase {
	public function testCanBeCreatedImageWithoutTitle(): void {
		$folder      = ASSETS_DIR . '/assets/images/';
		$image_name  = $folder . 'test.png';
		$image_title = '';
		$class_name  = 'img-fluid';
		$image       = "<img src=\"{$image_name}\" alt=\"{$image_title}\" class=\"{$class_name}\">";
		static::assertSame( $image, ( new Image( 'test.png' ) )->asset() );
	}

	public function testCanBeCreatedImageWithTitle(): void {
		$folder      = ASSETS_DIR . '/assets/images/';
		$image_name  = $folder . 'test.png';
		$image_title = 'test image';
		$class_name  = 'img-fluid';
		$image       = "<img src=\"{$image_name}\" alt=\"{$image_title}\" class=\"{$class_name}\">";
		static::assertSame( $image, ( new Image( 'test.png', $image_title ) )->asset() );
	}

	public function testCanBeCreatedImageWithClassname(): void {
		$folder      = ASSETS_DIR . '/assets/images/';
		$image_name  = $folder . 'test.png';
		$image_title = 'test image';
		$class_name  = 'rounded';
		$image       = "<img src=\"{$image_name}\" alt=\"{$image_title}\" class=\"img-fluid {$class_name}\">";
		static::assertSame( $image, ( new Image( 'test.png', $image_title, $class_name ) )->asset() );
	}
}
