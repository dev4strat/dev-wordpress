<?php
/**
 * Testing Text class.
 *
 * @package dev4stratTests
 * @since   2024
 */

use Inc\Ext\Utils\Text;
use PHPUnit\Framework\TestCase;
/**
 *
 */
class TextTest extends TestCase {

	/**
	 * @return void
	 */
	public function testCanBeRemovedAllSpaces(): void {
		$text1 = 'Hallo Mama, wie gehts';
		static::assertSame( 'HalloMama,wiegehts', Text::remove_spaces( $text1 ) );
	}

	/**
	 * @return void
	 */
	public function testCanBeRemovedAllSpacesOutsideTag(): void {
		$text2 = '     Hallo Mama, wie gehts    ';
		static::assertSame( 'Hallo Mama, wie gehts', Text::remove_all_spaces_outside_tag( $text2 ) );
	}

    /**
     * @return void
     */
    public function testCanBeRemovedSpacesOutsideTag(): void {
        $text2 = '     Hallo Mama, wie gehts    ';
        static::assertSame( ' Hallo Mama, wie gehts', Text::remove_spaces_outside_tag( $text2 ) );
    }
}
