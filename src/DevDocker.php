<?php
/**
 * Create docker with composer.
 *
 * @package devWordpress
 * @since   2024
 */

namespace Dev4strat\DevWordpress;

use Composer\Script\Event;
use Symfony\Component\Dotenv\Dotenv;

// phpcs:disable
class DevDocker
{
    public static string $project_path = "";

    public static function getRoot(Event $event): void {
        $composer = $event->getComposer();
        $package = $composer->getPackage();
        self::$project_path = './';
    }

    public static function loadEnv(): void
    {
        $dotenv = new Dotenv();
        $dotenv->load( self::$project_path . '.env' );
    }

    public static function startDocker(): void {
        self::docker("docker-compose start");
    }

    public static function stopDocker(Event $event): void
    {
        self::getRoot($event);
        self::loadEnv();

        $processes = array(
            "docker-compose stop",
            "docker-compose rm -f"
        );
        self::docker(implode( " && ", $processes ));
        DevHelper::remove_all(self::$project_path . "/out");
        DevHelper::remove_all(dirname( __DIR__ ) . '/../dist/docker/' . $_ENV[ 'NAME' ]);
    }

    public static function docker(string $run): void {
        DevHelper::println( $run );
        self::console( $run );
    }

    public static function clear(Event $event): void
    {
        self::getRoot($event);

        $processes = array(
            "docker-compose stop"
        );
        self::docker(implode( " && ", $processes ));
        DevHelper::remove_all(self::$project_path . "/out");
    }

    public static function phpcs(): void
    {
        self::console( "phpcs -p --standard=Wordpress ./app/src --extensions=php --ignore=index.php,polylang-slug.php --report=emacs" );
    }

    public static function phpcbf(): void
    {
        self::console( "phpcbf ./app/src --standard=Wordpress --extensions=php --colors --ignore=index.php,polylang-slug.php --report=summary" );
    }

    public static function copy( string $src, string $out ): void
    {
        DevHelper::copyFolder($src, $out);
    }

    public static function console(string $cmd): void {
        $out = shell_exec( $cmd );
        DevHelper::println( $out );
    }
}
