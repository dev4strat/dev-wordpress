<?php
/**
 * Wordpress interface for composer.
 *
 * @package devWordpress
 * @since   2024
 */

namespace Dev4strat\DevWordpress\Interfaces;

interface DevWordpressInterface extends DevInterface {
    /**
     * Copy Wordpress configuration file.
     *
     * @return void
     */
    public static function copyConfig(): void;

    /**
     * Copy activated theme.
     *
     * @return void
     */
    public static function copyThemes(): void;

    /**
     * Copy languages.
     *
     * @return void
     */
    public static function copyLanguages(): void;

    /**
     * Copy Font files.
     *
     * @return void
     */
    public static function copyFonts(): void;

    /**
     * Create uploads folder.
     *
     * @return void
     */
    public static function createUploads(): void;

    /**
     * Change version in config file.
     * Generate style.css file.
     *
     * @return void
     */
    public static function loadConfig(): void;

    /**
     * Load image files into upload folder.
     * Save image file name into DB.
     *
     * @return void
     */
    public static function loadImages(): void;

    /**
     * Remove unused Themes.
     *
     * @return void
     */
    public static function removeUnusedThemes(): void;
}
