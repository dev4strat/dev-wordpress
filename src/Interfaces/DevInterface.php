<?php
/**
 * Docker interface for composer.
 *
 * @package devWordpress
 * @since   2024
 */

namespace Dev4strat\DevWordpress\Interfaces;

interface DevInterface {
    /**
     * Stop docker.
     *
     * @return void
     */
    public static function stopDocker(): void;

    /**
     * Remove docker and remove target folder.
     *
     * @return void
     */
    public static function clear(): void;
}
