<?php
/**
 * Use wordpress cli import images.
 *
 * @package devWordpress
 * @since   2024
 */

namespace Dev4strat\DevWordpress;

// phpcs:disable
class DevWordpressCli {
    /**
     * Constructor.
     */
    public function __construct() {
        $this->runWpCli();
    }

    /**
     * Run cp_cli.
     *
     * @return void
     */
    private function runWpCli(): void
    {
        $new_files = $this->getAllFiles();

        if ( count( $new_files ) > 0 ) {
            foreach ( $new_files as $file ) {
                $file_path = $file->folder . '/' . $file->file;
                $output    = shell_exec( "wp media import --path='./out' $file_path" );
                DevHelper::println( $output );
            }
        }
    }

    /**
     * Get files of folders.
     *
     * @return array
     */
    private function getAllFiles(): array {
        $dir   = './app/src/assets/uploads';
        $files = $this->searchFiles( $dir, array() );

        $out_dir          = './out/wp-content/uploads';
        $out_files        = array_diff( scandir( $out_dir ), array( '..', '.' ) );
        $new_files        = array();
        $image_extensions = array( 'png', 'jpg', 'jpeg', 'gif', 'svg', 'webp' );

        foreach ( $files as $value ) {
            if ( ! in_array( $value->file, $out_files ) ) {
                $extension = pathinfo( $value->file, PATHINFO_EXTENSION );
                if ( in_array( $extension, $image_extensions ) ) {
                    $new_files[] = $value;
                }
            }
        }

        return $new_files;
    }

    /**
     * Search Files.
     *
     * @param string $dir Path.
     * @param array  $files Files.
     * @return array
     */
    private function searchFiles( string $dir, array $files ): array {
        $ffs = scandir( $dir );

        unset( $ffs[ array_search( '.', $ffs, true ) ] );
        unset( $ffs[ array_search( '..', $ffs, true ) ] );
        unset( $ffs[ array_search( '.DS_Store', $ffs, true ) ] );

        // prevent empty ordered elements.
        if ( count( $ffs ) < 1 ) {
            return array();
        }

        foreach ( $ffs as $ff ) {
            if ( is_dir( $dir . '/' . $ff ) ) {
                $subfolderFiles = $this->searchFiles( $dir . '/' . $ff, $files );
                $files           = $this->uniqueObjects( array_merge( $subfolderFiles, $files ), 'file' );
            } else {
                $files[] = (object) array(
                    'file'   => $ff,
                    'folder' => $dir,
                );
            }
        }

        return $files;
    }

    /**
     * Unique array object.
     *
     * @param array  $array Objects.
     * @param string $property Key.
     * @return array
     */
    private function uniqueObjects( array $array, string $property ): array {
        $temp_array = array_unique( array_column( $array, $property ) );
        return array_values( array_intersect_key( $array, $temp_array ) );
    }
}
