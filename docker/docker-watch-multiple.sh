#!/bin/bash

# Starte PHP-FPM im Hintergrund
php-fpm &
echo "PHP-FPM gestartet..."

# Warte, bis PHP-FPM läuft
sleep 5

WATCH_DIR="/var/www/src"
TARGET_DIR="/var/www/html/wp-content/themes"

# Definiere die Zielprojekte für "shared"
# shellcheck disable=SC2046
export $(grep -v '^#' .env | xargs)
# String aus .env in ein Array umwandeln
IFS=',' read -r -a SHARED_TARGETS <<< "$PROJECTS"

echo "Starte inotify-Dateiwächter für $WATCH_DIR..."
echo "Projekte: ${SHARED_TARGETS[*]}"

inotifywait -r -m "$WATCH_DIR" -e modify,create,delete |
while read -r path action file; do
    echo "Änderung erkannt: $path $action $file"

    # Entferne den Basisverzeichnis-Pfad
    RELATIVE_PATH="${path#"$WATCH_DIR/"}"

    # Extrahiere den ersten Ordnernamen (Projektname)
    PROJECT_NAME="${RELATIVE_PATH%%/*}"

    # Entferne "theme/" aus dem Pfad
    SYNC_PATH="${RELATIVE_PATH#"$PROJECT_NAME/theme/"}"

    echo "Projekt: $PROJECT_NAME"
    echo "Pfad: $SYNC_PATH$file"

    if [ "$PROJECT_NAME" = "shared" ]; then
        # Wenn das Projekt "shared" ist, synchronisiere die Datei in alle definierten Projekte
        for TARGET_PROJECT in "${SHARED_TARGETS[@]}"; do
            echo "Synchronisiere nach: $TARGET_DIR/$TARGET_PROJECT/$SYNC_PATH"
            rsync -av --delete --exclude="*.{tx,txs,scss}" "$path" "$TARGET_DIR/$TARGET_PROJECT/$SYNC_PATH"
        done
    else
        # Normale Synchronisation für reguläre Projekte
        echo "Synchronisiere nach: $TARGET_DIR/$PROJECT_NAME/$SYNC_PATH"
        rsync -av --delete --exclude="*.{tx,txs,scss}" "$path" "$TARGET_DIR/$PROJECT_NAME/$SYNC_PATH"
    fi
done
