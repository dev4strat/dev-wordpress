#!/bin/bash

# Starte PHP-FPM im Hintergrund
php-fpm &
echo "PHP-FPM gestartet..."

# Warte, bis PHP-FPM läuft
sleep 5

WATCH_DIR="/var/www/src/theme"
TARGET_DIR="/var/www/html/wp-content/themes"

# Definiere die Zielprojekte für "shared"
# shellcheck disable=SC2046
export $(grep -v '^#' .env | xargs)

echo "Starte inotify-Dateiwächter für $WATCH_DIR..."
echo "Projekte: ${PROJECTS}"

inotifywait -r -m "$WATCH_DIR" -e modify,create,delete |
while read -r path action file; do
    echo "Änderung erkannt: $path $action $file"

    # Entferne den Basisverzeichnis-Pfad
    RELATIVE_PATH="${path#"$WATCH_DIR/"}"

    # Entferne "theme/" aus dem Pfad
    SYNC_PATH="${RELATIVE_PATH#"/theme/"}"

    echo "Pfad: $SYNC_PATH$file"

    # Normale Synchronisation für reguläre Projekte
    echo "Synchronisiere nach: $TARGET_DIR/$PROJECTS/$SYNC_PATH"
    rsync -av --delete --exclude="*.{tx,txs,scss}" "$path" "$TARGET_DIR/$PROJECTS/$SYNC_PATH"

done
